var http = require('http');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var redis = require("redis");
var session = require('express-session');
var redisStore = require('connect-redis')(session);
var client = redis.createClient();
var soap = require('soap');

var index = require('./routes/index');
var login = require('./routes/login');
var logout = require('./routes/logout');
var project = require('./routes/project');

var app = express();

/*
 SOAP SERVER CONFIG HERE -------------
 */
var xml = require('fs').readFileSync(path.join(__dirname, 'services', 'myserv.wsdl'), 'utf8');
var soapService = require("./services/soap");

app.listen(8001, function () {
    //Note: /wsdl route will be handled by soap module
    //and all other routes & middleware will continue to work
    soap.listen(app, '/wsdl', soapService, xml);
});
/*-------------------------------------------------*/

//Redis session implementation
app.use(session({
    secret: 'ssshhhhh',
    store: new redisStore({
        host: 'localhost',
        port: 6379,
        client: client,
        ttl: 2600
    }),
    saveUninitialized: false,
    resave: false
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.post('/login', login);
app.post('/logout', logout);
app.post('/project/add', project);
app.post('/project/update/:id', project);
app.post('/project/remove/:id', project);
app.post('/wsdl/:id', project);
app.get('/project', project);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
