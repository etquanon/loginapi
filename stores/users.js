"use strict";

var md5 = require("md5");
var db = require("../config/db");

function getUser (params, callback) {
    var sql = "SELECT * FROM user WHERE email = ? and password = ?";

    db.query(sql, [params.email, md5(params.password)], function (err, res) {
        if (err) return callback(err);

        callback(null, res.pop());
    });
}

module.exports = {
    getUser: getUser
};
