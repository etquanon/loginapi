"use strict";

var db = require("../config/db");

function getProjects (callback) {
    var sql = "SELECT * FROM project";

    db.query(sql, function (err, res) {
        if (err) return callback(err);

        callback(null, res);
    });
}

function insertProject (params, callback) {
    var sql = "INSERT INTO project SET ?";

    db.query(sql, params, function (err, res) {
        if (err) return callback(err);

        callback(null, res.insertId);
    });
}

function updateProject (set, where, callback) {
    var sql = "UPDATE project SET ? WHERE ?";

    db.query(sql, [set, where], function (err, res) {
        if (err) return callback(err);

        callback(null, res.changedRows);
    });
}

function deleteProject (params, callback) {
    var sql = "DELETE FROM project WHERE ?";

    db.query(sql, [ { id: params.id } ], function (err, res) {
        if (err) return callback(err);

        callback(null, res.affectedRows);
    });
}

module.exports = {
    getProjects: getProjects,
    insertProject: insertProject,
    updateProject: updateProject,
    deleteProject: deleteProject
};
