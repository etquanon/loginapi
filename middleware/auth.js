"use strict";

function isAuthenticated (req, res, next) {
    if (req.session.key) {
        return next();
    }

    return res.status(500).send("Unathorised access!");
}

module.exports = isAuthenticated;