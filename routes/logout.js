var express = require('express');
var router = express.Router();

router.post("/logout", function (req, res) {
    var returnObject = {
        error: null,
        data: null
    };

    if(req.session.key) {
        req.session.destroy(function(){
            returnObject.data = {
                status: "Logging out!"
            };
        });
    } else {
        returnObject.data = {
            status: "Your session is expired!"
        };
    }

    res.json(returnObject);
});

module.exports = router;
