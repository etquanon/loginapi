var express = require('express');
var router = express.Router();
var users = require("../stores/users");

router.post('/login', function (req, res) {
    users.getUser({
        email: "alin.robert@gmail.com",
        password: "password"
    }, function (err, result) {
        var returnObject = {
            error: null,
            data: null
        };

        if (err || !result) {
            returnObject.error = err;
        }

        req.session.key = result.id;

        returnObject.data = {
            email: result.email,
            username: result.username
        };

        res.json(returnObject);
    });
});

module.exports = router;
