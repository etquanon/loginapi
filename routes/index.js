var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    if (req.session.key) {
        res.json({
            error: null,
            data: "Welcome user! You are logged in!"
        });
    } else {
        res.json({
            error:null,
            data: "You are not logged in!"
        });
    }
});

module.exports = router;
