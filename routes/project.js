var express = require("express");
var router = express.Router();
var projects = require("../stores/projects");
var auth = require('../middleware/auth');

router.post('/wsdl/:id', auth, function (req, res) {
    var returnObject = {
        error: null,
        data: null
    };

    projects.updateProject({
        name: req.body.name,
        description: req.body.description
    }, {
        id: req.params.id,
    }, function (error, result) {
        if (error) {
            returnObject.error = error;
        }

        returnObject.data = {
            updated: result > 0
        };
        res.json(returnObject);
    });
});

router.get('/project', auth, function (req, res) {
    var returnObject = {
        error: null,
        data: null
    };

    projects.getProjects(function (error, result) {
        if (error) {
            returnObject.error = error;
        }

        returnObject.data = result;
        res.json(returnObject);
    });
});

router.post('/project/add', auth, function (req, res) {
    var returnObject = {
        error: null,
        data: null
    };

    projects.insertProject({
        name: req.body.name,
        description: req.body.description
    }, function (error, result) {
        if (error) {
            returnObject.error = error;
        }

        returnObject.data = {
            inserted: result
        };
        res.json(returnObject);
    });
});

router.post('/project/update/:id', auth, function (req, res) {
    var returnObject = {
        error: null,
        data: null
    };

    projects.updateProject({
        name: req.body.name,
        description: req.body.description
    }, {
        id: req.params.id,
    }, function (error, result) {
        if (error) {
            returnObject.error = error;
        }

        returnObject.data = {
            updated: result > 0
        };
        res.json(returnObject);
    });
});

router.post('/project/remove/:id', auth, function (req, res) {
    var returnObject = {
        error: null,
        data: null
    };

    if (req.body.erase) {
        projects.deleteProject({
            id: req.params.id
        }, function (error, result) {
            if (error) {
                returnObject.error = error;
            }

            returnObject.data = {
                erased: result > 0
            };
            res.json(returnObject);
        });
    } else {
        projects.updateProject({
            active: 0
        }, {
            id: req.params.id,
        }, function (error, result) {
            if (error) {
                returnObject.error = error;
            }

            returnObject.data = {
                updated: result > 0
            };
            res.json(returnObject);
        });
    }
});

module.exports = router;