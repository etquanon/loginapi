"use strict";

var myService = {
    MyService: {
        MyPort: {
            // This is how to define an asynchronous function.
            UpdateProject: function(args, callback) {
                console.log("SOAP -->", args.name);
                // do some work
                callback({
                    name: args.name
                });
            }
        }
    }
};

module.exports = myService;